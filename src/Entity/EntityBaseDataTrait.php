<?php

namespace Drupal\re_mgr\Entity;

/**
 * Provide base const data for operations on Real Estate Manager entities.
 */
trait EntityBaseDataTrait {

  /**
   * Entity list.
   */
  public static array $entityList = [
    're_mgr_estate',
    're_mgr_building',
    're_mgr_floor',
    're_mgr_flat',
  ];

  /**
   * Map of parent entity names.
   */
  protected static array $parentEntityMap = [
    're_mgr_flat' => 're_mgr_floor',
    're_mgr_floor' => 're_mgr_building',
    're_mgr_building' => 're_mgr_estate',
  ];

  /**
   * Map of related entity names.
   */
  protected static array $relatedEntityMap = [
    're_mgr_estate' => 're_mgr_building',
    're_mgr_building' => 're_mgr_floor',
    're_mgr_floor' => 're_mgr_flat',
  ];

  /**
   * Contains all flat statuses.
   */
  public static array $entitiesStatuses = [
    1 => 'Available',
    2 => 'Reserved',
    3 => 'Sold',
  ];

  /**
   * Contains statuses colors.
   */
  public static array $statusesColor = [
    1 => 'green',
    2 => 'orange',
    3 => 'red',
  ];

}
